'use strict';

const staticRoot = './assets'

module.exports = {
  src: {
    css: staticRoot + '/src/css/**/**',
    img: staticRoot + '/src/img/**/**',
    js: staticRoot + '/src/js/**/*.js',
    fonts: staticRoot + 'src/fonts/**/**'
  },
  dest: {
    css: staticRoot + '/dest/css/',
    img: staticRoot + '/dest/img/',
    js: staticRoot + '/dest/js/',
    fonts: staticRoot + 'dest/fonts/'
  }
};
