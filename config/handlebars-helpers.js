'use strict';

module.exports = {
  handlebarsHelpers: {
    convertToJSON: function(object) {
      return JSON.stringify(object);
    },
    installmentPrice: function() {
      return (this.price/this.installments).toFixed(2).replace('.', ',');
    },
    formatPrice: function(price) {
      return price.toFixed(2).replace('.', ',');
    }
  }
};
