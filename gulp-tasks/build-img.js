'use strict';

const gulp = require('gulp');
const imageMin = require('gulp-imagemin');
const paths = require('../config/paths');

module.exports = {
  fn: function(gulp, callback) {
    return gulp.src(paths.src.img)
      .pipe(imageMin())
      .pipe(gulp.dest(paths.dest.img));
  }
};
