'use strict';

const gulp = require('gulp');
const postCSS = require('gulp-postcss');
const sourceMaps = require('gulp-sourcemaps');
const cssNano = require('gulp-cssnano');
const autoprefixer = require('autoprefixer');
const fontMagician = require('postcss-font-magician');
const gulpIf = require('gulp-if');
const gulpUtil = require('gulp-util');
const paths = require('../config/paths');

module.exports = {
  fn: function(gulp, callback) {
    return gulp.src(paths.src.css)
      .pipe(gulpIf(gulpUtil.env.type !== 'production', sourceMaps.init()))
      .pipe(postCSS([
        autoprefixer({browsers: ['last 2 versions']}),
        fontMagician({variants: {
            'Open Sans': {
              '400': ['woff, eot, woff2'],
              '600': ['woff, eot, woff2'],
              '700': ['woff, eot, woff2']
            }
          }
        })
      ]))
      .pipe(gulpIf(gulpUtil.env.type === 'production', cssNano()))
      .pipe(gulpIf(gulpUtil.env.type !== 'production', sourceMaps.write()))
      .pipe(gulp.dest(paths.dest.css))
  }
};
