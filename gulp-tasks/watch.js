'use strict';

const gulp = require('gulp');
const gulpWatch = require('gulp-watch');
const paths = require('../config/paths');

module.exports = {
  fn: function(gulp, callback) {
    gulpWatch(paths.src.css, () => gulp.start('build-css'));
    gulpWatch(paths.src.img, () => gulp.start('build-img'));
    gulpWatch(paths.src.js, () => gulp.start('build-js'));
  }
};
