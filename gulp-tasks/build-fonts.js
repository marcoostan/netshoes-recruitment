'use strict';

const gulp = require('gulp');
const paths = require('../config/paths');

module.exports = {
  fn: function(gulp, callback) {
    return gulp.src(paths.src.fonts)
      .pipe(gulp.dest(paths.dest.fonts));
  }
};
