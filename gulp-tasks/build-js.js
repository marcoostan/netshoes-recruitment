'use strict';

const gulp = require('gulp');
const gulpIf = require('gulp-if');
const gulpUtil = require('gulp-util');
const gulpUglify = require('gulp-uglify');
const paths = require('../config/paths');

module.exports = {
  fn: function(gulp, callback) {
    return gulp.src(paths.src.js)
      .pipe(gulpIf(gulpUtil.env.type === 'production', gulpUglify()))
      .pipe(gulp.dest(paths.dest.js))
  }
};
