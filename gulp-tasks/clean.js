'use strict';

const gulp = require('gulp');
const del = require('del');

module.exports = {
  fn: function(gulp, callback) {
    return del(['assets/dest']);
  }
};
