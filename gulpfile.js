'use strict';

const gulpRequireTasks = require('gulp-require-tasks');
const gulp = require('gulp');

gulpRequireTasks({
  path: process.cwd() + '/gulp-tasks'
});

gulp.task('default', ['build-css', 'build-img', 'build-js']);
