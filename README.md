# Netshoes Recruitment Test

This is a test to Front-end Developer Recruitment Test.

# Getting Started

Follow the instructions bellow.

 - Run `npm install` to download project dependencies.
 - Run `npm run build` to build project.
 - Run `npm run start` to initialize the web projet and open in your local host [http://localhost:300](http://localhost:300)
