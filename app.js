const express = require('express');
const expressHbs = require('express-handlebars');
const hbsHelpers = require('./config/handlebars-helpers');
const products = require(__dirname + '/data/products.json');
const app = express();

app.engine('.hbs', expressHbs({
  defaultLayout: 'index',
  layoutsDir: 'views',
  extname: '.hbs',
  helpers: hbsHelpers.handlebarsHelpers
}));

app.set('view engine', '.hbs');
app.use('/assets/dest', express.static(__dirname + '/assets/dest'));

app.get('/', function(req, res) {
  res.render('index', products);
});

app.listen(3000);
