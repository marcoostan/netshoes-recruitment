(function NetshoesCart () {
  function ControlCart (context) {
    this.context = document.querySelectorAll(context)
    document.addEventListener('DOMContentLoaded', this.init(this.context))
  }

  ControlCart.prototype.init = function (context) {
    var products = context
    var _this = this

    products.forEach(function (current) {
      current.addEventListener('click', _this.addProductToCart.bind(_this))
    })
  }

  ControlCart.prototype.getProductData = function (element) {
    return this.parseProductData(element.getAttribute('data-product'))
  }

  ControlCart.prototype.parseProductData = function (productInfo) {
    return JSON.parse(productInfo)
  }

  ControlCart.prototype.addProductToCart = function (event) {
    event.preventDefault()
    var productData = this.getProductData(event.currentTarget)

    this.openBag()

    console.log(productData)
    console.log('product added!')
  }

  ControlCart.prototype.openBag = function () {
    document.querySelector('.bag-content').classList.add('is-bag-open')
  }

  return new ControlCart('.product-link')
}).call(this)
